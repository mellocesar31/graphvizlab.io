---
defaults:
- '0.25'
flags: []
minimums:
- '0.02'
name: nodesep
types:
- double
used_by: G
---
In `dot`, `nodesep` specifies the minimum space between two adjacent nodes in the same rank, in inches.

For other layouts, `nodesep` affects the spacing between loops on a single node, or multiedges between
a pair of nodes.
