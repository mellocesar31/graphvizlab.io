---
defaults:
- 'false'
flags: []
minimums: []
name: landscape
types:
- bool
used_by: G
---
If true, the graph is rendered in landscape mode. Synonymous with
[`rotate`](#d:rotate)`=90` or [`orientation`](#d:orientation)`=landscape`.
